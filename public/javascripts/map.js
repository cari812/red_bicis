

var map = L.map('main_map').setView([35.28732848952404, -82.44461949027945], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//L.marker([35.28732848952404, -82.44461949027945]).addTo(map)
//L.marker([35.2871763691882, -82.4460180243176]).addTo(map)
//L.marker([35.29185889288468, -82.42919169604765]).addTo(map)

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function (result) {
        console.log(result);
        result.bicicletas.forEach(function (bici) {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(map);
        });
    }
})
