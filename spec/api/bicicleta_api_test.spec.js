var mongoose = require('mongoose')
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
var base_url = "http://localhost:5000/api/bicicletas"


describe('Bicicleta API', () => {

    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'mongoDB connection error: '));
        db.once('open', function () {
            console.log('we are connected to test database');
            done();
        });


    });
    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });

    });


    describe('GET BICICLETAS /', () => {
        it('Status 200 vacia', (done) => {
            request.get(base_url, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                Bicicleta.allBicis(function (err, doc) {
                    expect(doc.length).toBe(0);
                    done();
                });
            });
        });
        it('Status 200 con uno', (done) => {
            var a = new Bicicleta({ code: 10, color: 'rojo', modelo: 'urbana', ubicacion: [-34, -54] });
            Bicicleta.add(a, function (doc) {
                request.get(base_url, function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    Bicicleta.allBicis(function (err, doc) {
                        expect(doc.length).toBe(1);
                        done();
                    });
                });
            });
        });
    });


    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = { "id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 };
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: JSON.stringify(aBici)
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe(aBici.color);
                expect(bici.modelo).toBe(aBici.modelo);
                expect(bici.ubicacion[0]).toBe(aBici.lat);
                expect(bici.ubicacion[1]).toBe(aBici.lng);
                done();
            });
        });
    });

    describe('POST BICICLETAS /delete', () => {
        it('Status 204', (done) => {
            var headers = { 'content-type': 'application/json' };
            var a = new Bicicleta({ code: 10, color: 'rojo', modelo: 'urbana', ubicacion: [-34, -54] });
            var aBiciId = { "id": a.code };
            Bicicleta.add(a);

            expect(Bicicleta.allBicis.length).toBe(1);

            request.post({
                headers: headers,
                url: base_url + '/delete',
                body: JSON.stringify(aBiciId)
            }, function (error, response, body) {
                expect(response.statusCode).toBe(204);
                Bicicleta.allBicis(function (err, doc) {
                    expect(doc.length).toBe(0);
                    done();
                });
            });
        });
    });

    describe('POST BICICLETAS /update', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var a = new Bicicleta({ code: 10, color: 'rojo', modelo: 'urbana', ubicacion: [-34, -54] });
            Bicicleta.add(a, function () {
                var headers = { 'content-type': 'application/json' };
                var updatedBici = { "id": a.code, "color": "verde", "modelo": "montaña", "lat": -33, "lng": -55 };
                request.post({
                    headers: headers,
                    url: base_url + '/update',
                    body: JSON.stringify(updatedBici)
                }, function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var foundBici = Bicicleta.findByCode(10, function (err, doc) {
                        expect(doc.code).toBe(10);
                        expect(doc.color).toBe(updatedBici.color);
                        expect(doc.modelo).toBe(updatedBici.modelo);
                        expect(doc.ubicacion[0]).toBe(updatedBici.lat);
                        expect(doc.ubicacion[1]).toBe(updatedBici.lng);
                        done();
                    });
                });
            });
        });
    });

});











/* beforeEach(function () { console.log('testeando…'); });
describe('Bicicleta API', () => {

    describe('Get Bicicleta/', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var aBici = new Bicicleta(1, 'rojo', 'urbana', [35.28659284970101, -82.44419033685081]);
            Bicicleta.add(aBici);

            request.get('http://localhost:5000/api/Bicicletas', function (error, response, body) {
                expect(response.statusCode).toBe(200)
            })

        })
    })
    describe(' POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            expect(Bicicleta.allBicis.length).toBe(1);
            var headers = { 'content-type': 'application/json' };

            var abici = '{ "id":8,"color":"Green","modelo":"Urbana","lat": 10.975832,"lng": -74.808815 }';

            request.post({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/create',
                body: abici
            },
                function (error, response, body) {

                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(8).color).toBe('Green');
                    done();
                });

        });
    });

    describe('POST BICICLETAS /update', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var abici = '{"id": 8, "color": "violeta", "modelo": "urbano", "lat": -35, "lng": -28}';
            request.post({
                headers: headers,
                url: 'http://localhost:5000/bicicletas/8/update',
                body: abici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(302);
                expect(Bicicleta.findById(8).color).toBe("violeta");
                done();
            });
        });
    });

    describe('POST BICICLETAS /delete', () => {
        it('Status 200', (done) => {
            expect(Bicicleta.allBicis.length).toBe(2);

            var headers = { 'content-type': 'application/json' };
            var abici = '{"id": 8}';
            request.post({
                headers: headers,
                url: 'http://localhost:5000/bicicletas/8/delete',
                body: abici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(302);
                expect(Bicicleta.allBicis.length).toBe(1);
                done();
            });
        });
    });

}) */