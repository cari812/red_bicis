var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function () {

    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'mongoDB connection error: '));
        db.once('open', function () {
            console.log('we are connected to test database');
            done();
        });


    });
    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });

    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [35.28659284970101, -82.44419033685081]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(35.28659284970101);
            expect(bici.ubicacion[1]).toBe(-82.44419033685081);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });

        });

    });

    describe('Bicicleta.add', () => {
        it('agrega  una bici a la colección', (done) => {
            var aBici = new Bicicleta({ code: 1, color: "rosa", modelo: "montaña", ubicacion: [35.28659284970101, -82.44419033685081] });
            Bicicleta.add(aBici, function (err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Retorna la bici con code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code: 1, color: "rosa", modelo: "urbana", ubicacion: [35.28659284970101, -82.44419033685081] });
                Bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.log(err);

                    Bicicleta.add(aBici, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toEqual(aBici.color);
                            expect(targetBici.modelo).toEqual(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('Eliminar la bici con code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code: 1, color: "rosa", modelo: "urbana", ubicacion: [35.28659284970101, -82.44419033685081] });
                Bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.log(err);

                    Bicicleta.add(aBici, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.allBicis(function (err, bicis) {
                            expect(bicis.length).toBe(1);
                            Bicicleta.removeByCode(1, function (error, response) {
                                Bicicleta.allBicis(function (err, bicis) {
                                    expect(bicis.length).toBe(0);
                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    });





})
/* var Bicicleta = require('../../models/bicicleta');

beforeEach(() => { Bicicleta.allBicis = [] })

describe('Bicicleta.albicis', () => {
    it('Comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
    })
})

describe('Bicicleta.add', () => {
    it('Agregmos una bici', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'violeta', 'urbana', [35.29185889288468, -82.42919169604765]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    })
})
describe('Bicicleta.findbyId', () => {
    it('Debe retornar la bici con id=1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, 'violeta', 'urbana', [35.29185889288468, -82.42919169604765]);
        var bBici = new Bicicleta(2, 'blanca', 'montaña', [35.2871763691882, -82.4460180243176]);
        Bicicleta.add(aBici);
        Bicicleta.add(bBici);

        var targetBici = Bicicleta.findById(1)
        expect(targetBici.id).toBe(1)
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    })
})
describe('Bicicleta.removebyId', () => {
    it('Debe eliminar la bici con id=1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, 'violeta', 'urbana', [35.29185889288468, -82.42919169604765]);
        var bBici = new Bicicleta(2, 'roja', 'montaña', [35.29185889288468, -82.42919169604765]);
        Bicicleta.add(aBici);
        Bicicleta.add(bBici);
        expect(Bicicleta.allBicis.length).toBe(2);
        Bicicleta.removeById(1)
        expect(Bicicleta.allBicis.length).toBe(1);
        var targetBici = Bicicleta.findById(2)
        expect(targetBici.color).toBe(bBici.color);
        expect(targetBici.modelo).toBe(bBici.modelo)
    })
}) */